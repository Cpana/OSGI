package de.thkoeln.eksa.osgi.anwendung;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import de.thkoeln.eksa.osgi.verwaltung.KundeKontoVerwaltung;
import org.osgi.framework.ServiceRegistration;

public class ActivatorAnwendung implements BundleActivator {


    ServiceReference[] kkverwaltung;

    @Override
    public void start(BundleContext bundleContext) throws Exception {

        kkverwaltung = bundleContext.getServiceReferences(
                KundeKontoVerwaltung.class.getName(), "(Verwaltung=*)");
        if(kkverwaltung != null){
            Anwendung anwendung = new Anwendung((KundeKontoVerwaltung) bundleContext.getService(kkverwaltung[0]));
            anwendung.main();
        }

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        // NOTE: The service is automatically unregistered.
        // source von der NOTE --> https://felix.apache.org/documentation/tutorials-examples-and-presentations/apache-felix-osgi-tutorial/apache-felix-tutorial-example-2.html

    }
}
