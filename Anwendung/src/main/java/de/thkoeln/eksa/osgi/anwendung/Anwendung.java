package de.thkoeln.eksa.osgi.anwendung;

import de.thkoeln.eksa.osgi.entitaetsklassen.Konto;
import de.thkoeln.eksa.osgi.entitaetsklassen.Kunde;
import de.thkoeln.eksa.osgi.verwaltung.KundeKontoVerwaltung;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.util.ArrayList;

public class Anwendung  {

    KundeKontoVerwaltung kundeKontoVerwaltungsService;
    public Anwendung(KundeKontoVerwaltung service) {
        this.kundeKontoVerwaltungsService = service;
    }

    public void main(){
        System.out.println("__________________________________________________");

        int kunde1 = kundeKontoVerwaltungsService.neuerKunde("Tim");
        System.out.println("Kunde Tim hat Kundennummer: " + kunde1);

        int kunde2 = kundeKontoVerwaltungsService.neuerKunde("Stephan");
        System.out.println("Kunde Stephan hat Kundennummer: " + kunde2);
        System.out.println("__________________________________________________");
        int konto1 = kundeKontoVerwaltungsService.neuesKonto();
        System.out.println("Kontonummer von Konto 1: " + konto1);

        int konto2 = kundeKontoVerwaltungsService.neuesKonto();
        System.out.println("Kontonummer von Konto 2: " + konto2);

        int konto3 = kundeKontoVerwaltungsService.neuesKonto();
        System.out.println("Kontonummer von Konto 3: " + konto3);

        Kunde kundenObjekt1 = kundeKontoVerwaltungsService.getKunde(kunde1);
        Kunde kundenObjekt2 = kundeKontoVerwaltungsService.getKunde(kunde2);

        kundeKontoVerwaltungsService.besitztKonto(kundenObjekt1, konto1);
        kundeKontoVerwaltungsService.besitztKonto(kundenObjekt1, konto2);
        kundeKontoVerwaltungsService.besitztKonto(kundenObjekt2, konto3);
        System.out.println("__________________________________________________");
        ArrayList<Konto> timskonten = kundeKontoVerwaltungsService.alleKonten(kundenObjekt1);
        System.out.println("Alle gefundenen Konten von Tim: ");
        for (Konto k : timskonten) {
            System.out.print(k.getKontonr());
            System.out.println();
        }
        System.out.println("__________________________________________________");
        ArrayList<Konto> stephansKonten = kundeKontoVerwaltungsService.alleKonten(kundenObjekt2);
        System.out.println("Alle gefundenen Konten von Stephan: ");
        for (Konto k : stephansKonten) {
            System.out.print(k.getKontonr());
            System.out.println();
        }
    }
}