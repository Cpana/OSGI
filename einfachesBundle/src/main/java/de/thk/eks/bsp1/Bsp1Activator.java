package de.thk.eks.bsp1;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Bsp1Activator implements BundleActivator {


    public void start(BundleContext context)
    {
        System.out.println("Bundle Bsp1 gestartet.");

    }


    public void stop(BundleContext context)
    {
        System.out.println("Bundle Bsp1 gestoppt.");
    }


}
