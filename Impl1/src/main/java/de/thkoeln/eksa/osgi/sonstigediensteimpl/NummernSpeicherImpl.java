package de.thkoeln.eksa.osgi.sonstigediensteimpl;

import de.thkoeln.eksa.osgi.sonstigedienste.NummernSpeicher;

import java.util.ArrayList;
import java.util.List;

public class NummernSpeicherImpl implements NummernSpeicher {
    private static int kundenId = 0;
    private static int kontoId =0;

    public int getNeueKontoNr(){
        kundenId++;
        return kundenId;
    }
    public int getNeueKundenNr(){
        kontoId++;
        return kontoId;
    }


}
