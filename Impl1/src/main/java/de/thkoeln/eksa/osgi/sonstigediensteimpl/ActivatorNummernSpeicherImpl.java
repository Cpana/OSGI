package de.thkoeln.eksa.osgi.sonstigediensteimpl;

import de.thkoeln.eksa.osgi.sonstigedienste.NummernSpeicher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Hashtable;

public class ActivatorNummernSpeicherImpl implements BundleActivator
{
    private ServiceRegistration nummernSpeicherService;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        Hashtable<String, String> props = new Hashtable<>();
        props.put("Verwaltung","Nummern");
        bundleContext.registerService(NummernSpeicher.class.getName(), new NummernSpeicherImpl(), props);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        // NOTE: The service is automatically unregistered.
        // source von der NOTE --> https://felix.apache.org/documentation/tutorials-examples-and-presentations/apache-felix-osgi-tutorial/apache-felix-tutorial-example-2.html

    }
}
