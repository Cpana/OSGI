package de.thkoeln.eksa.osgi.starter;

import org.apache.felix.framework.Felix;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

import java.util.HashMap;
import java.util.Map;

public class Starter {

        public static void main(String[] args) {
            try {
                // Initialize Apache Felix Framework
                Map<String, String> configMap = new HashMap<String, String>();
                configMap.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit");
                Felix framework = new Felix(configMap);
                framework.init();

                BundleContext context = framework.getBundleContext();

                // Start and stop framework and bundles
                framework.start();
                System.out.println("Staus nach dem Install -> 32 für erfolgreich");
                System.out.println("Staus nach dem Uninstall -> 1 für erfolgreich");

                System.out.println(" >> basisklassen: install <<");
                Bundle bsp1 = context.installBundle("file:basisklassen/target/basisklassen-1.0.jar");
                System.out.println(" >> basisklassen: start <<");
                bsp1.start();
                System.out.println("Status des Bundles " + bsp1.getState() + " nach install");

                System.out.println(" >> impl1: install <<");
                Bundle bsp2 = context.installBundle("file:Impl1/target/impl1-1.0.jar");
                System.out.println(" >> impl1: start <<");
                bsp2.start();
                System.out.println("Status des Bundles " + bsp2.getState() + " nach install");

                System.out.println(" >> impl2: install <<");
                Bundle bsp3 = context.installBundle("file:Impl2/target/impl2-1.0.jar");
                System.out.println(" >> impl2: start <<");
                bsp3.start();
                System.out.println("Status des Bundles " + bsp3.getState() + " nach install");

                System.out.println(" >> anwendung: install <<");
                Bundle anwendung = context.installBundle("file:Anwendung/target/anwendung-1.0.jar");
                System.out.println(" >> anwendung: start <<");
                anwendung.start();

                //Uninstall alle Bundles
                bsp1.uninstall();
                bsp2.uninstall();
                bsp3.uninstall();
                anwendung.uninstall();
                System.out.println("__________________________________________________");
                System.out.println("Status des Bundles (Basisklassen) nach uninstall --> " + bsp1.getState());
                System.out.println("Status des Bundles (Impl1) nach uninstall --> " + bsp2.getState());
                System.out.println("Status des Bundles (Impl2) nach uninstall --> " + bsp3.getState());
                System.out.println("Status des Bundles (Anwendung) nach uninstall --> " + anwendung.getState());

                System.out.println(">> ENDE Starter <<");

                framework.stop();
            }
            catch (Exception exception) {
                System.err.println("Error while executing program: " + exception);
                exception.printStackTrace();
                System.exit(0);
            }

        }
    }
