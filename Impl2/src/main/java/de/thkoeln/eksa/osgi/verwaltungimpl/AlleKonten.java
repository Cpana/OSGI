package de.thkoeln.eksa.osgi.verwaltungimpl;

import de.thkoeln.eksa.osgi.entitaetsklassen.Konto;

import java.util.ArrayList;

public class AlleKonten {
    private static AlleKonten instance;
    private ArrayList<Konto> konten;

    private AlleKonten()
    {
        konten = new ArrayList<>();
    }
    public static AlleKonten getInstance() {
        if(AlleKonten.instance == null) {
            AlleKonten.instance= new AlleKonten();
        }
        return AlleKonten.instance;
    }

    public boolean addKonto(Konto neuKonto) {
        return konten.add(neuKonto);
    }

    public ArrayList<Konto> getKonten() {
        return konten;
    }
}
