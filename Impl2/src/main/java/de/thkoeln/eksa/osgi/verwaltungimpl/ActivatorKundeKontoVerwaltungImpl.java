package de.thkoeln.eksa.osgi.verwaltungimpl;

import java.util.Hashtable;

import de.thkoeln.eksa.osgi.sonstigedienste.NummernSpeicher;
import de.thkoeln.eksa.osgi.verwaltung.KundeKontoVerwaltung;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class ActivatorKundeKontoVerwaltungImpl implements BundleActivator {

        ServiceReference[] serviceReferences;

        @Override
        public void start(BundleContext bundleContext) throws Exception {
            Hashtable<String, String> props = new Hashtable<>();

            serviceReferences = bundleContext.getServiceReferences(
                    NummernSpeicher.class.getName(), "(Verwaltung=*)");
            props.put("Verwaltung","Klasse");
            bundleContext.registerService(KundeKontoVerwaltung.class.getName(), new KundeKontoVerwaltungImpl(), props);
        }

        @Override
        public void stop(BundleContext bundleContext) throws Exception {
            // NOTE: The service is automatically unregistered.
            // source von der NOTE --> https://felix.apache.org/documentation/tutorials-examples-and-presentations/apache-felix-osgi-tutorial/apache-felix-tutorial-example-2.html

        }

    }
