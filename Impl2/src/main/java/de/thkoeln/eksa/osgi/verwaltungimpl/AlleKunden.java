package de.thkoeln.eksa.osgi.verwaltungimpl;

import de.thkoeln.eksa.osgi.entitaetsklassen.Kunde;

import java.util.ArrayList;
import java.util.List;

public class AlleKunden
{
    private static AlleKunden instance;
    private List<Kunde> kunden;

    private AlleKunden()
    {
        kunden = new ArrayList<>();
    }
    public static AlleKunden getInstance() {
        if(AlleKunden.instance == null) {
            AlleKunden.instance= new AlleKunden();
        }
        return AlleKunden.instance;
    }

    public boolean add(Kunde neuKunde) {
        return kunden.add(neuKunde);
    }

    public Kunde getKunde(int kundenr) {
        for(Kunde kunde: kunden){
            if (kunde.getKundennummer() == kundenr){
                return kunde;
            }
        }
        return null;
    }
}
