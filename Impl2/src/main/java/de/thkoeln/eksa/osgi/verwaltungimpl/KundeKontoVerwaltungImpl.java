package de.thkoeln.eksa.osgi.verwaltungimpl;

import de.thkoeln.eksa.osgi.entitaetsklassen.Konto;
import de.thkoeln.eksa.osgi.entitaetsklassen.Kunde;
import de.thkoeln.eksa.osgi.sonstigedienste.NummernSpeicher;
import de.thkoeln.eksa.osgi.verwaltung.KundeKontoVerwaltung;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.util.ArrayList;

public class KundeKontoVerwaltungImpl implements KundeKontoVerwaltung {


    ArrayList<Kunde> kunden;

     KundeKontoVerwaltungImpl() {
        kunden = new ArrayList<>();
    }

    /**
     * Erzeugt ein neues Objekt der Klasse Kunde
     * Hierbei muss auf das Interface NummernSpeicher
     * zugegriffen werden, um eine neue Kundennummer zu erhalten.
     */
    @Override
    public int neuerKunde(String name) {
        Kunde kunde;
        BundleContext bundleContext = null;
        bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        ServiceReference referencesNummernSpeicher = null;
        referencesNummernSpeicher = bundleContext.getServiceReference(NummernSpeicher.class.getName());
        NummernSpeicher np = (NummernSpeicher) bundleContext.getService(referencesNummernSpeicher);
        AlleKunden alleKunden = AlleKunden.getInstance();
        int kundenNr = np.getNeueKundenNr();
        kunde = new Kunde();
        kunde.setKundennummer(kundenNr);
        kunde.setName(name);
        if(!alleKunden.add(kunde)) {
            return -1;
        }
        return kundenNr;
    }

    /**
     * Das Kundenobjekt zur gegebenen Kundennummer wird
     * zurückgegeben.
     *
     * @param kundenr Kundennummer des gesuchten Kundenobjekts
     * @return Objekt der Klasse Kunde mit der Kundenummer kundenr
     */
    @Override
    public Kunde getKunde(int kundenr) {
        AlleKunden alleKunden = AlleKunden.getInstance();
        Kunde kunde = alleKunden.getKunde(kundenr);
        return kunde;
    }

    /**
     * Liefert alle Konten des gegebenen Kunden.
     *
     * @param k Kundenobjekt, für das die Konten
     *          ermittelt werden sollen
     * @return Liste aller Konten des Kunden k.
     */
    @Override
    public ArrayList<Konto> alleKonten(Kunde k) {
        ArrayList<Konto> rueckgabe = new ArrayList<>();
        ArrayList<Konto> alleKonten = AlleKonten.getInstance().getKonten();
        for (Konto konto : alleKonten) {
            if (k.getKonten().contains(konto.getKontonr())) {
                rueckgabe.add(konto);
            }
        }
        return rueckgabe;
    }

    /**
     * Es wird der Kundenverwaltung mitgeteilt, dass
     * der Kunde k nun das Konto mit der
     * Kontonummer kontonr besitzt.
     *
     * @param k       Kunde, der das neue Konto besitzt
     * @param kontonr Kontonummer des neuen Kontos des Kunden k
     */
    @Override
    public void besitztKonto(Kunde k, int kontonr) {
        k.getKonten().add(kontonr);

    }

    /**
     * Erzeugt ein neues Objekt der Klasse Konto.
     * Hierbei muss auf das Interface NummernSpeicher
     * zugegriffen werden, um eine neue Kontonummer zu erhalten.
     *
     * @return vergebene Kontonummer
     *
     */
    @Override
    public int neuesKonto() {
        Konto konto = new Konto();
        AlleKonten alleKonten = AlleKonten.getInstance();
        BundleContext bundleContext = null;
        bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        ServiceReference referencesNummernSpeicher = null;
        referencesNummernSpeicher = bundleContext.getServiceReference(NummernSpeicher.class.getName());
        NummernSpeicher np = (NummernSpeicher) bundleContext.getService(referencesNummernSpeicher);
        int rueckgabe = np.getNeueKontoNr();
        konto.setKontonr(rueckgabe);
        alleKonten.addKonto(konto);
        return rueckgabe;

    }
}
