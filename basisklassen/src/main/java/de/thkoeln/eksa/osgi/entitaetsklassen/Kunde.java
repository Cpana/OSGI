package de.thkoeln.eksa.osgi.entitaetsklassen;

import java.util.ArrayList;

public class Kunde
{
    private String name;                // Name des Kunden
    private int kundennummer;           // eindeutige Kundennummer
    private ArrayList<Integer> konten;  // Liste der Kontonummern aller  Konten des Kunden

    public Kunde() { konten = new ArrayList<>(); }

    public void addKonto (int kontonr)
    {
        konten.add(kontonr);
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name) { this.name = name; }
    public int getKundennummer()
    {
        return kundennummer;
    }
    public void setKundennummer(int kundennummer)
    {
        this.kundennummer = kundennummer;
    }
    public ArrayList<Integer> getKonten()
    {
        return konten;
    }
}
